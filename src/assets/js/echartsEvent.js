/** 开放外部方法 */
import echarts from 'echarts'
export {drawChart}

/** 获取父组件元素 */
var chart;
// 基于准备好的dom，初始化echarts实例
var myChart;
//设置全局变量
var events = [];
var toolbox

/** 可配置变量 */
const data = {
    singleChartHeight: 300, //单个折线图图的高度
    singleAllChartHeight: 400, //单个折线图整体高度
    chart2top: 80, //整个折线图距离顶部高度
    chart2bot: 20, //整个折线图距离底部高度
    chart2left: 50, //整个折线图距离左侧高度
    chart2right: 50, //整个折线图距离右侧高度
    colorA: 0.8, //rgba的透明度
    event: {
    	width: 180,
    	height: 50
    }
}

/** 公用option */
const option = {
    title: {},
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            animation: false
        }
    },
    legend: {
        data:[],
        x: 'left'
    },
    axisPointer: {
        link: {xAxisIndex: 'all'}
    },
    dataZoom: [
        {
            show: true,
            realtime: false,
            start: 0,
            end: 100,
            xAxisIndex: [0, 1]
        },
        {
            type: 'inside',
            realtime: false,
            start: 0,
            end: 100,
            xAxisIndex: [0, 1]
        }
    ],
    grid: [],
    xAxis: [],
    yAxis: [],
    series: []
};

/** 画图，核心方法 */
const drawChart = function(dataInput) {
	//初始化data数据
	initData(dataInput)
	//初始化事件
	initEvent(dataInput)
	//调整折线图总体高度
	initChart(dataInput)
	// 绘制图表
	setTimeout(() => {
    console.log(option)
		myChart.setOption(option);
	},300)

	//添加监听事件
	addListen(dataInput)
}

//-----主体方法 start------------

/** 添加监听事件 **/
const addListen = function(dataInput) {
  var dateStart = dataInput.xAxisData[0]
  var dateEnd = dataInput.xAxisData[dataInput.xAxisData.length - 1]
  var dateChange = timeCompare(dateEnd, dateStart)
  var xStart = data.chart2left
  var xEnd = document.getElementById('echarts').offsetWidth - xStart - data.chart2right
  //监听折线图放大缩小
  myChart.on('datazoom', function(params){
    var start = 0, end = 0
    if (dateChange > 0) {
      if ('start' in params) {
        start = params.start
        end = params.end
      } else {
        start = params.batch[0].start
        end = params.batch[0].end
      }
      //重新选择事件位置
      for (var i in events) {
        var x = timeCompare(events[i].time, dateStart) / dateChange * 100
        if ((x >= start) && (x <= end)) {
          events[i].left = (x - start) / (end - start) * (xEnd - xStart) + xStart - data.event.width / 2
          if (open) {
            events[i].children[0].invisible = false
            events[i].children[1].invisible = false
          }
          events[i].clickFlag = true
        } else {
          events[i].children[0].invisible = true
          events[i].children[1].invisible = true
          events[i].clickFlag = false
        }
      }
      myChart.setOption({graphic: events})
    }
  })
  myChart.on('restore', function(params){
    for (var i in events) {
  		if (dateChange > 0) {
        if ((timeCompare(events[i].time, dateStart) >= 0) && (timeCompare(dateEnd, events[i].time) >= 0)) {
          var time2X = timeCompare(events[i].time, dateStart)
          events[i].left = time2X / dateChange * (xEnd - xStart) + xStart - data.event.width / 2
          events[i].clickFlag = true
          if (open) {
            events[i].children[0].invisible = false
            events[i].children[1].invisible = false
          }
        } else {
          events[i].children[0].invisible = true
          events[i].children[1].invisible = true
          events[i].clickFlag = false
        }
      } else {
        events[i].children[0].invisible = true
        events[i].children[1].invisible = true
        events[i].clickFlag = false
      }
  	}
    myChart.setOption({graphic: events})
  })
}

/** 初始化data数据 */
const initData = function(dataInput) {
	//配置title
	option.title = {
        text: dataInput.title.text,
        subtext: dataInput.title.subtext,
        x: 'center'
	}
	//遍历初始化各项内容
	for (var i = 0; i < dataInput.chartNum; i ++) {
		//配置x轴
		option.xAxis[i] = {
			gridIndex: i,
            type: 'category',
            boundaryGap: false,
            axisLine: {onZero: true},
            data: dataInput.xAxisData
        }
        //配置每个折线图的位置
        option.grid[i] = {
	        left: data.chart2left,
	        right: data.chart2right,
	        top: data.chart2top + data.singleAllChartHeight * i,
	        height: data.singleChartHeight
	    }
	    //配置y轴
	    option.yAxis[i] = {
            gridIndex: i,
            name : dataInput.yAxisName[i],
            type : 'value'
        }
	    for (var j = 0; j < dataInput.series[i].length; j++) {
	    	//配置图例
	    	option.legend.data[i * dataInput.chartNum + j] = dataInput.series[i][j].name
	        //配置折线
	    	option.series[i * dataInput.chartNum + j] = {
	            name: dataInput.series[i][j].name,
	            type:'line',
	            xAxisIndex: i,
	            yAxisIndex: i,
	            symbolSize: 8,
	            hoverAnimation: false,
	            data: dataInput.series[i][j].data
	        }
	    }
	}
  option.graphic = events
}

/** 初始化事件 */
const initEvent = function(dataInput) {
	var eventInput = dataInput.event
	var count = 0 //第几个事件
	var maxZ = 5
  var minZ = 3
	for (var i in eventInput) {
		//遍历事件
		var toTop = 0
		var toTopIncrease = 10
		for (var j in eventInput[i]) {
			var event = {
				type: 'group',
				top: data.chart2top + i * data.singleAllChartHeight + data.singleChartHeight - data.event.height - toTop,
				time: eventInput[i][j].date,
				clickFlag: false,
				children:
				[
					{
						type: 'rect',
						z: minZ,
						left: 'center',
						top: 'middle',
						shape: {
							width: data.event.width,
							height: data.event.height
						},
						style: {
                fill: getRandomColor(),
                lineWidth: 2,
                shadowBlur: 8,
                shadowOffsetX: 3,
                shadowOffsetY: 3,
                shadowColor: 'rgba(0,0,0,0.3)'
            },
            index: count, //表明是第几个事件
            invisible: false,
            onmouseover: function() {
            	events[this.index].children[0].z = maxZ;
                events[this.index].children[1].z = maxZ;
                myChart.setOption({graphic: events})
            },
            onmouseout: function() {
                events[this.index].children[0].z = minZ;
                events[this.index].children[1].z = minZ;
                myChart.setOption({graphic: events})
            }
					},
					{
						type: 'text',
            z: minZ,
            left: 'center',
            top: 'middle',
            style: {
                fill: '#fff',
                text: eventInput[i][j].title + '\n\n' + eventInput[i][j].content,
                font: '12px Microsoft YaHei'
            },
            invisible: false,
            index: count,
            url: eventInput[i][j].url,
            onmouseover: function() {
                events[this.index].children[0].z = maxZ;
                events[this.index].children[1].z = maxZ;
                myChart.setOption({graphic: events})
            },
            onmouseout: function() {
                events[this.index].children[0].z = minZ;
                events[this.index].children[1].z = minZ;
                myChart.setOption({graphic: events})
            },
            onclick:function(){
            	window.open(this.url)
            }
					}
				]
			}
			events.push(event)
  		toTop += toTopIncrease
			count ++
		}
	}
	/** 处理所有事件位置 */
	events = dealEventLocation(events, dataInput)
  toolbox = {
      feature: {
          restore: {},
          saveAsImage: {},
          dataView: {
            readOnly: true
          },
          myTool: {
            show: true,
            title: '关闭事件',
            icon: 'image://https://dn-vbuluo-static.qbox.me/%E5%BC%802.png',
            onclick: function() {
              if (toolbox.feature.myTool.icon == 'image://https://dn-vbuluo-static.qbox.me/%E5%BC%802.png') {
                  toolbox.feature.myTool.icon = 'image://https://dn-vbuluo-static.qbox.me/%E5%85%B32.png';
                  toolbox.feature.myTool.title = '打开事件'
              } else {
                  toolbox.feature.myTool.icon = 'image://https://dn-vbuluo-static.qbox.me/%E5%BC%802.png'
                  toolbox.feature.myTool.title = '关闭事件'
              }
              open=!open
              for(var i in events){
                if(events[i].clickFlag==true){
                  events[i].children[0].invisible=!events[i].children[0].invisible
                  events[i].children[1].invisible=!events[i].children[1].invisible
                }
              }
              myChart.setOption({graphic: events,toolbox:toolbox})
            }
          }
      }
  }
  option.toolbox = toolbox
}

/** 处理所有事件位置 */
const dealEventLocation = function(events, dataInput) {
  var dateStart = dataInput.xAxisData[0]
  var dateEnd = dataInput.xAxisData[dataInput.xAxisData.length - 1]
  var dateChange = timeCompare(dateEnd, dateStart)
  var xStart = data.chart2left
  var xEnd = document.getElementById('echarts').offsetWidth - xStart - data.chart2right
	for (var i in events) {
		if (dateChange > 0) {
      if ((timeCompare(events[i].time, dateStart) >= 0) && (timeCompare(dateEnd, events[i].time) >= 0)) {
        var time2X = timeCompare(events[i].time, dateStart)
        events[i].left = time2X / dateChange * (xEnd - xStart) + xStart - data.event.width / 2
        events[i].clickFlag = true
      } else {
        events[i].children[0].invisible = true
        events[i].children[1].invisible = true
      }
    } else {
      events[i].children[0].invisible = true
      events[i].children[1].invisible = true
    }
	}
  return events
}

/** 初始化折线图 */
const initChart = function(dataInput) {
	//因为必须重新画div才能渲染出折线图
	var height = data.chart2top
		+ data.singleAllChartHeight * dataInput.chartNum
		+ data.chart2bot
	document.getElementById('echarts').innerHTML =
		'<div id="myChart" style="width:100%;height:'
		+ height +'px;"></div>'
	chart = document.getElementById('myChart')
	myChart = echarts.init(chart)
	chart.style.height = height + 'px'
}



//--------- 工具方法start -------

/** 获取随机rgb颜色 */
var getRandomColor = function() {
    var r = Math.floor(Math.random() * 256);
    var g = Math.floor(Math.random() * 256);
    var b = Math.floor(Math.random() * 256);
    return "rgba(" + r + ',' + g + ',' + b + ',' + data.colorA + ")";
}

var timeCompare = function(time1, time2) {
    // FIXME: 时间比较、代码不健壮
    //time1 >time2 使用
    if(time1.length<9){
      time1+='-01'
    }
    if(time2.length<9){
      time2+='-01'
    }
    var timeSplited1=time1.split('-')
    var timeSplited2=time2.split('-')
    var timeChaValue=0;
    timeChaValue=(timeSplited1[0]-timeSplited2[0])*360
    if(timeSplited1[1]>timeSplited2[1]){
      timeChaValue+=(timeSplited1[1]-timeSplited2[1])*30
    }
    else{
      timeChaValue-=(timeSplited2[1]-timeSplited1[1])*30
    }
    if(timeSplited1[2]>timeSplited2[2]){
      timeChaValue+=(timeSplited1[2]-timeSplited2[2])
    }
    else{
      timeChaValue-=(timeSplited2[2]-timeSplited1[2])
    }
    return timeChaValue
}
